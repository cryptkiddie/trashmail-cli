# trashmail-cli

A cli for trashmail.org

This allows you, to create trashmail.com-adresses from commandline

Run ```./trashmail-cli.sh --help``` for more information

## ToDo
 - Fix Multidomain-Support
 - Include check for curl (dependency)
 - allow signup
 - detect unhandled errors
 - better argument support

## Disclaimer
This software is neither written by trashmail.com nor do they know it exists

I'm not liable for anything you break :-)

It did not run through a propper test
