#!/bin/bash

function dependencies {
	curl -V 2>1 >/dev/null || echo "Curl is required to run application" >/dev/stderr

}

function query {
curl --data  "form_source=$1&form_domain:$2&form_dest=$3&form_nb_redirections=$4&form_expire_days=$5&form_whitelisting=$6&delete_msg_chk=$7&fe-mobile=$8&create_submit=$9" 'https://trashmail.com/?lang=us' 
}

function resultparser {
status=9
echo "$1" |grep -o '<a class="btn btn-primary btn-lg" href="/?lang=en" role="button">Create new one</a>' >/dev/null && status=0
echo "$1" |grep -o 'Sorry. The source name' >/dev/null && status=1
echo "$1" |grep -o 'The email address is not associated with a registered Plus customer.' >/dev/null && status=2
echo "$1" |grep -o 'Your real email address is not registered.' >/dev/null && status=3
echo $status
##!! > /dev/stderr
}

function humanreadable {
 case $1 in
    0)
        echo "Email sucessfully created"
        ;;
    1)
        echo "Fake Email already taken"
        ;;
    2)
        echo 'Your configuration is only available to plus customers'
        ;;
    3)
        echo "You are not registered, please go to https://trashmail.com and register your email"
	;;
    9)
	echo "unknown error, please submit on https://gitlab.com/cryptkiddie/trashmail-cli"
	;;
    *)  echo "Something is terribly wrong here"
esac
}

function debug {
    echo "$result" > /tmp/trashmail-cli-result.html
    echo "Wrote result to /tmp/trashmail-cli-result.html"
}

function printhelp {
    echo "trashmail-cli help"
    echo "-----------------"
    echo "trashmail-cli is an interface for trashmail.com"
    echo ""
    echo "Arguments"
    echo "---"
    echo "Mandatory:"
    echo "-tac --trash-account-name Accountname of trashmail to create. i.E. \"foo\" for \"foo@trashmail.com\""
    echo "-dst --destination Email Adress to forward to. i.E. example@example.org" 
    echo ""
    echo "optional:"
    echo "-dom --domain Domain of trashmail. (not yet implemented) Default: trashmail.com"
    echo "-fwd --forwards Amount of Emails to forward. Max. 10 for free, -1 for infinit (plus only) Default: 10"
    echo "-days --days-active Maxium days to last. Max. 31 for free, -1 for infinit (plus only) Default: 31"
    echo "-cpt --captcha Turns CAPTCHA on (default off)"
    echo "-nodelmsg --no-delete-message Don't send message on expire"
    echo "-debug --write-debug-file Saves result from trashmail.com"
    
}
# Predefine Variabes
dom="trashmail.com"
fwd="10"
days="31"
cpt="0"
delmsg="1"
# Argparse
for i in "$@"
do
case $i in
    -h|--help)
        printhelp
        exit 0
        ;;
    -tac=*|--trash-account-name=*)
        tac="${i#*=}"
        ;;
    -dst=*|--destination=*)
        dst="${i#*=}"
        ;;
    -dom=*|--domain=*)
        dom="${i#*=}"
        ;;
    -fwd=*|--forward=*)
        fwd="${i#*=}"
        ;;
    -days=*|--days-active=*)
        days="${i#*=}"
        ;;
    -cpt|--captcha)
        cpt=3
        ;;
    -nodelmsg|--no-delete-message)
        delmsg=0;
        ;;
    -debug|--write-debug-file)
	debugging="true"
	;;
    *)
       echo "Unkown option $i" >/dev/stderr
       exit 4
    ;;
esac
done
dependencies
result=$(query "$tac" "$dom" "$dst" "$fwd" "$days" "$cpt" "$delmsg" "true" "true")
if [[ "$debugging" == "true" ]]
then
	debug
fi

resultint=$(resultparser "$result")
humanreadable "$resultint"
exit "$resultint"
